Name:           hiredis
Version:        1.2.0
Release:        3
Summary:        A minimalistic C client library for the Redis database
License:        BSD-3-Clause
URL:            https://github.com/redis/hiredis
Source0:        https://github.com/redis/hiredis/archive/refs/tags/v%{version}.tar.gz#/hiredis-1.2.0.tar.gz
BuildRequires:  gcc redis
BuildRequires:  openssl-devel

Patch0001:      hiredis-1.2.0-fix-linkage-of-ssl-library.patch
Patch0002:      fix-memory-uninitialized-in-fuzz-testcase.patch
Patch0003:      Retry-poll-2-if-we-are-intterupted.patch
Patch0004:      Document-poll-2-logic-changes.patch
Patch0005:      Fix-memory-leak.patch

%description
Hiredis is a minimalistic C client library for the Redis database.
It is minimalistic because it just adds minimal support for the protocol,
but at the same time it uses a high level printf-alike API in order to make it much higher level than
otherwise suggested by its minimal code base and the lack of explicit bindings for every Redis command.

Apart from supporting sending commands and receiving replies, it comes with a reply parser that
is decoupled from the I/O layer.It is a stream parser designed for easy reusability, which can
for instance be used in higher level language bindings for efficient reply parsing.

%package        devel
Summary:        Development files for hiredis
Requires:       hiredis = %{version}-%{release}
Requires:       openssl-devel

%description    devel
The hiredis-devel package contains development files to build applications for hiredis.

%prep
%autosetup -p1

%build
%make_build \
  PREFIX="%{_prefix}" \
  LIBRARY_PATH="%{_lib}" \
  OPTIMIZATION="" \
  CFLAGS="%{?build_cflags}" \
  LDFLAGS="%{?build_ldflags}" \
  PLATFORM_FLAGS="%{?build_cflags} %{?build_ldflags}" \
  USE_SSL=1

%install
%make_install PREFIX="%{_prefix}" LIBRARY_PATH="%{_lib}" USE_SSL=1
%delete_la_and_a

%check
make check

%files
%license COPYING
%{_libdir}/libhiredis.so.*
%{_libdir}/libhiredis_ssl.so.*

%files devel
%doc CHANGELOG.md README.md
%{_includedir}/hiredis/
%{_libdir}/libhiredis.so
%{_libdir}/libhiredis_ssl.so
%{_libdir}/pkgconfig/hiredis.pc
%{_libdir}/pkgconfig/hiredis_ssl.pc

%changelog
* Sun Feb 02 2025 Funda Wang <fundawang@yeah.net> - 1.2.0-3
- Really use pan system build flags
- build ssl library

* Wed Aug 7 2024 zhangxingrong <zhangxingrong@uniontech.cn> - 1.2.0-2
- Retry poll(2) if we are intterupted
- Document poll(2) logic changes
- Fix memory leak


* Tue Aug 22 2023 Ge Wang <wang__ge@126.com> - 1.2.0-1
- Update to version 1.2.0

* Thu Jul 20 2023 zhangchenglin <zhangchenglin@kylinos.cn> - 1.1.0-1
- Update to version 1.1.0

* Fri Dec 16 2022 xu_ping <xuping33@h-partners.com> - 1.0.2-3
- fix memory uninitialized in fuzz testcase

* Tue May 24 2022 loong_C <loong_c@yeah.net> - 1.0.2-2
- fix spec changelog date

* Mon Oct 11 2021 houyingchao<houyingchao@huawei.com> - 1.0.2-1
- Fix CVE-2021-32765

* Fri Jun  4 2021 lingsheng<lingsheng@huawei.com> - 0.13.3-12
- fix heap buffer overflow in redisvFormatCommand

* Tue Mar 17 2020 likexin<likexin4@huawei.com> - 0.13.3-11
- fix up cve-2020-7105

* Tue Dec 31 2019 liujing<liujing144@huawei.com> - 0.13.3-10
- Package init
